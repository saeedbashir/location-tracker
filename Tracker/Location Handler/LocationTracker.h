//
//  LocationTracker.h
//  Tracker
//
//  Created by Saeed Bashir on 3/18/15.
//  Copyright (c) 2015 Saeed Bashir. All rights reserved.
//

#import "BackgroundTaskManager.h"
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationTracker : NSObject <CLLocationManagerDelegate>
{
    CLLocation *desiredLocation;
    int count;
}

@property (nonatomic) CLLocationCoordinate2D myLastLocation;
@property (nonatomic) CLLocationAccuracy myLastLocationAccuracy;
@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;
@property (nonatomic) BackgroundTaskManager * bgTask;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) NSTimer * delay10Seconds;

+ (CLLocationManager *)sharedLocationManager;

- (void)startLocationTracking;
- (void)stopLocationTracking;
- (void)updateLocationToServer;


@end
