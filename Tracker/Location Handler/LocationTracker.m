//
//  LocationTracker.m
//  Tracker
//
//  Created by Saeed Bashir on 3/18/15.
//  Copyright (c) 2015 Saeed Bashir. All rights reserved.
//

#import "LocationTracker.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
# define LOCATION_RESTART_TIMER 60

@implementation LocationTracker

+ (CLLocationManager *)sharedLocationManager {
	static CLLocationManager *_locationManager;
	
	@synchronized(self) {
		if (_locationManager == nil) {
			_locationManager = [[CLLocationManager alloc] init];
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
		}
	}
	return _locationManager;
}

- (id)init {
	
    if (self == [super init])
    {
        //register notification for when app enter in background mode
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
	}
	return self;
}

//This method called when app enter in background because its register for UIApplicationDidEnterBackgroundNotification
-(void)applicationEnterBackground{
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    if(IS_OS_8_OR_LATER) {
        [locationManager requestAlwaysAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    //Use the BackgroundTaskManager to manage all the background Task
    self.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.bgTask beginNewBackgroundTask];
}

//fire this method with n seconds so that app will never be idle for 3 minutes in background and updates location after n seconds. If app will not update location before 3 minutes in background then app will not be able to get location in background
- (void) restartLocationUpdates
{
    //invalide and free memory for previous schedule timer
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    //location manager settings
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    if(IS_OS_8_OR_LATER) {
        [locationManager requestAlwaysAuthorization];
    }
    
    [locationManager startUpdatingLocation];
}

//fire this method when you want to get user location
- (void)startLocationTracking {

	if ([CLLocationManager locationServicesEnabled] == NO)
    {
		UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[servicesDisabledAlert show];
	}
    else
    {
        CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
        
        if(authorizationStatus == kCLAuthorizationStatusDenied || authorizationStatus == kCLAuthorizationStatusRestricted){
            NSLog(@"authorizationStatus failed");
        }
        else
        {
            CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            
            if(IS_OS_8_OR_LATER) {
                [locationManager requestAlwaysAuthorization];
            }
            
            [locationManager startUpdatingLocation];
        }
    }
}

//fire this method when no longer need of user location
- (void)stopLocationTracking {
    
    if (self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
    }
    
	CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
	[locationManager stopUpdatingLocation];
}

//Stop the locationManager for battery saving purpose
-(void)stopLocationDelayBy10Seconds{
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    [locationManager stopUpdatingLocation];
}

//fire this method after n minutes when you want to send userlocation to server
- (void)updateLocationToServer {
   	// you have updated your location in desiredLocation
   	NSLog (@"lat: %f long: %f",desiredLocation.coordinate.latitude,desiredLocation.coordinate.longitude);
   	
   	if (desiredLocation.horizontalAccuracy > 1000) {
        // Accuracy is not good :(
    }
    
    // send location to server
    
    
    // count and loal notification added so that user know app is running in background
    count +=1;
    
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    notification.repeatInterval = NSCalendarUnitDay;
    [notification setAlertBody:[NSString stringWithFormat:@"lat: %f long: %f and Notification No: %d",desiredLocation.coordinate.latitude,desiredLocation.coordinate.longitude,count]];
    [notification setFireDate:[NSDate dateWithTimeIntervalSinceNow:1]];
    [notification setTimeZone:[NSTimeZone  defaultTimeZone]];
    
    [[UIApplication sharedApplication] setScheduledLocalNotifications:[NSArray arrayWithObject:notification]];
}


#pragma mark - CLLocationManagerDelegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        desiredLocation = currentLocation;
    }
    
    //If the timer still valid, return it no need to run below code
    if (self.timer) {
        return;
    }
    
    self.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.bgTask beginNewBackgroundTask];
    
    //Restart the locationMaanger after n minute
    self.timer = [NSTimer scheduledTimerWithTimeInterval:LOCATION_RESTART_TIMER target:self
                                                selector:@selector(restartLocationUpdates)
                                                userInfo:nil
                                                 repeats:NO];
    
    //Will only stop the locationManager after 10 seconds, so that we can get some accurate locations
    //The location manager will only operate for 10 seconds to save battery
    if (self.delay10Seconds) {
        [self.delay10Seconds invalidate];
        self.delay10Seconds = nil;
    }
    
    self.delay10Seconds = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                         selector:@selector(stopLocationDelayBy10Seconds)
                                                         userInfo:nil
                                                          repeats:NO];
    
}

- (void)locationManager: (CLLocationManager *)manager didFailWithError: (NSError *)error
{
    switch([error code])
    {
        case kCLErrorNetwork: // general, network-related error
        {
            NSLog(@"Please check your network connection.");
        }
            break;
        case kCLErrorDenied:{
            NSLog(@"You have to enable the Location Service to use this App. To enable, please go to Settings->Privacy->Location Services");
        }
            break;
        default:
        {
            
        }
            break;
    }
}


@end
