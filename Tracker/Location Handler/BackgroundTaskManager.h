//
//  BackgroundTaskManager.h
//  Tracker
//
//  Created by Saeed Bashir on 3/18/15.
//  Copyright (c) 2015 Saeed Bashir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackgroundTaskManager : NSObject

+(instancetype)sharedBackgroundTaskManager;

-(UIBackgroundTaskIdentifier)beginNewBackgroundTask;
-(void)endAllBackgroundTasks;

@end
