//
//  AppDelegate.h
//  Tracker
//
//  Created by Saeed Bashir on 3/18/15.
//  Copyright (c) 2015 Saeed Bashir. All rights reserved.
//

#import "LocationTracker.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)LocationTracker * locationTracker;
@property (strong, nonatomic) NSTimer* locationUpdateTimer;


@end

